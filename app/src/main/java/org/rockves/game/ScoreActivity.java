package org.rockves.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ScoreActivity extends Activity {

    short score = 0;
    String category;

    Button menu = null;
    Button restart = null;
    TextView text_score = null;
    ImageView medale = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        menu = (Button)this.findViewById(R.id.menu_button);
        restart = (Button)this.findViewById(R.id.restart_button);
        text_score = (TextView)this.findViewById(R.id.text_score);
        medale = (ImageView)this.findViewById(R.id.medal_image);
        // Set fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //intent get
        Intent intent = getIntent();
        score = intent.getShortExtra("score",(short)0);
        category = intent.getStringExtra("category");

        text_score.setText(Short.toString(score));
        //set medal
        if(score<=(Short.parseShort(getString(R.string.do_ilu_braz)))){
            medale.setImageResource(R.drawable.medalbraz);
        }else if(score<=(Short.parseShort(getString(R.string.do_ilu_srebro)))){
            medale.setImageResource(R.drawable.medalsrebro);
        }else{
            medale.setImageResource(R.drawable.medalzloto);
        }


        menu.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                //create intent
                Intent myIntent = new Intent(ScoreActivity.this, MainActivity.class);
                finish();
                ScoreActivity.this.startActivity(myIntent);
            }
        });

        restart.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                //create intent
                Intent myIntent = new Intent(ScoreActivity.this, GameActivity.class);
                myIntent.putExtra("category", category);
                myIntent.putExtra("time", getIntent().getLongExtra("time", 0));
                finish();
                ScoreActivity.this.startActivity(myIntent);
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            Intent myIntent = new Intent(ScoreActivity.this, MainActivity.class);
            finish();
            ScoreActivity.this.startActivity(myIntent);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}
