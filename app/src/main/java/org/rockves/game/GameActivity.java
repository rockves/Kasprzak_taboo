package org.rockves.game;

import android.app.Activity;
import java.util.concurrent.TimeUnit;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.Random;

public class GameActivity extends Activity {

    XmlParser xmlparser = null;
    int[] rand_tab;
    ImageButton next = null;
    ImageButton cancel = null;
    ImageButton button_menu = null;
    Button start = null;
    Button resume = null;
    Button exit = null;
    TextView text_score = null;
    TextView text_timer = null;
    TextView text_word = null;
    TextView forbidden_1 = null;
    TextView forbidden_2 = null;
    TextView forbidden_3 = null;
    TextView forbidden_4 = null;
    TextView forbidden_5 = null;
    ConstraintLayout game_ui = null;
    ConstraintLayout menu = null;
    ProgressBar time_bar = null;
    CountDownTimer count_down_timer;

    boolean penalty_points;
    String category;
    long time;
    short word_number = 0;
    short score = 0;
    short minutes = 0;
    short seconds = 0;
    Random rand;

    void settingsLoad(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        penalty_points = preferences.getBoolean(getString(R.string.penalty_points), false);
    }

    public String getValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }

    void next_word(){
        Element element = xmlparser.elements.get(rand_tab[word_number]);
        text_word.setText(getValue("text", element));
        forbidden_1.setText(getValue("forbidden_word_1", element));
        forbidden_2.setText(getValue("forbidden_word_2", element));
        forbidden_3.setText(getValue("forbidden_word_3", element));
        forbidden_4.setText(getValue("forbidden_word_4", element));
        forbidden_5.setText(getValue("forbidden_word_5", element));
        word_number++;
        if(word_number == 20){
            null_rand_tab();
            fill_rand_tab(xmlparser.elements.size());
            word_number = 0;
        }
    }

    void null_rand_tab(){
        for(int i : rand_tab){
            i = 0;
        }
    }

    void fill_rand_tab(int bound) {
        int n;
        int ile = Integer.parseInt(getString(R.string.rand_tab_size));
        for (int j = 0; j < ile; j++) {
            while (true) {
                n = rand.nextInt(bound);
                if (!(check_in_rand_tab(n))) {
                    rand_tab[j] = n;
                    break;
                }
            }
        }
    }

    boolean check_in_rand_tab(int x){
        for (int i : rand_tab) {
            if (i == x) return true;
        }
        return false;
    }

    void tick_second(){
        if(seconds == 0){
            minutes--;
            seconds = 59;
        }else{
            seconds--;
        }
    }

    InputStream file_by_category(String cat){
        switch(cat){
            case "basic":
                return (getResources().openRawResource(R.raw.category_basic));
            case "kasprzak":
                break;
            case "food":
                return (getResources().openRawResource(R.raw.category_food));
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        //get intent
        Intent intent = getIntent();
        time = intent.getLongExtra("time",(long)0);
        category = intent.getStringExtra("category");

        text_score = (TextView)this.findViewById(R.id.text_score);
        text_score.setText(Short.toString(score));
        next = (ImageButton)this.findViewById(R.id.button_next);
        cancel = (ImageButton)this.findViewById(R.id.button_cancel);
        button_menu = (ImageButton)this.findViewById(R.id.button_menu);
        start = (Button)this.findViewById(R.id.start_game_button);
        resume = (Button)this.findViewById(R.id.button_resume);
        exit = (Button)this.findViewById(R.id.button_exit);
        text_timer = (TextView)this.findViewById(R.id.timer);
        text_word = (TextView)this.findViewById(R.id.text_word);
        forbidden_1 = (TextView)this.findViewById(R.id.forbidden_1);
        forbidden_2 = (TextView)this.findViewById(R.id.forbidden_2);
        forbidden_3 = (TextView)this.findViewById(R.id.forbidden_3);
        forbidden_4 = (TextView)this.findViewById(R.id.forbidden_4);
        forbidden_5 = (TextView)this.findViewById(R.id.forbidden_5);
        rand_tab = new int[Integer.parseInt(getString(R.string.rand_tab_size))];
        game_ui = (ConstraintLayout)this.findViewById(R.id.game_ui);
        menu = (ConstraintLayout)this.findViewById(R.id.menu);
        time_bar = (ProgressBar)this.findViewById(R.id.timer_bar);
        time_bar.setMax((int)TimeUnit.MILLISECONDS.toSeconds(time));
        time_bar.setProgress((int)TimeUnit.MILLISECONDS.toSeconds(time));

        game_ui.setVisibility(View.GONE);

        // Set fullscreen

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //settings load
        settingsLoad();

        InputStream inputstr = file_by_category(category);
        xmlparser = new XmlParser(inputstr);
        rand = new Random();
        fill_rand_tab(xmlparser.elements.size());

        start.setOnClickListener(new ImageButton.OnClickListener(){
            @Override
            public void onClick(View v){
                game_ui.setVisibility(View.VISIBLE);
                start.setVisibility(View.GONE);
                count_down_timer.start();
            }
        });

        button_menu.setOnClickListener(new ImageButton.OnClickListener(){
            @Override
            public void onClick(View v){
                menu.setVisibility(View.VISIBLE);
                count_down_timer.pause();
            }
        });

        resume.setOnClickListener(new ImageButton.OnClickListener(){
            @Override
            public void onClick(View v){
                menu.setVisibility(View.GONE);
                count_down_timer.resume();
            }
        });

        exit.setOnClickListener(new ImageButton.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent menuIntent = new Intent(GameActivity.this, MainActivity.class);
                finish();
                startActivity(menuIntent);
            }
        });

        next.setOnClickListener(new ImageButton.OnClickListener(){
            @Override
            public void onClick(View v){
                next_word();
                score++;
                text_score.setText(Short.toString(score));
            }
        });
        cancel.setOnClickListener(new ImageButton.OnClickListener(){
            @Override
            public void onClick(View v){
                if(penalty_points){
                    score--;
                    text_score.setText(Short.toString(score));
                }
                next_word();
            }
        });
        next_word();

        minutes = (short)(time/60000);
        //timer
        count_down_timer = new CountDownTimer(time, 1000) {


            public void onTick(long millisUntilFinished) {

                tick_second();
                if(seconds>=10){
                    text_timer.setText(minutes+":"+seconds);
                }else {
                    text_timer.setText(minutes+":0"+seconds);
                }
                time_bar.setProgress((time_bar.getProgress() - 1));
                if((minutes == 0) && (seconds == 10)){
                    text_timer.setTextColor(Color.RED);
                }
            }

            public void onFinish() {
                Intent myIntent = new Intent(GameActivity.this, ScoreActivity.class);
                myIntent.putExtra("score", score);
                myIntent.putExtra("category", category);
                myIntent.putExtra("time", getIntent().getLongExtra("time",0));
                finish();
                GameActivity.this.startActivity(myIntent);
            }
        };
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            if(menu.getVisibility() == View.GONE){
                menu.setVisibility(View.VISIBLE);
                count_down_timer.pause();
            }else{
                menu.setVisibility(View.GONE);
                count_down_timer.resume();
            }

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}
