package org.rockves.game;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.transition.Slide;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

    Button first_option;
    Button second_option;
    Button thirth_option;
    Button fourth_option;
    ImageButton button_report;
    ImageButton button_exit_settings;
    LinearLayout choose_menu;
    ConstraintLayout menu_layout;
    ConstraintLayout category_layout;
    ConstraintLayout options_layout;
    Button button_category_basic;
    Button button_category_kasprzak;
    Button button_category_food;
    CheckBox checkBox_points_penalty;

    String category;
    boolean penalty_points = false;

    void settingsLoad(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        penalty_points = preferences.getBoolean(getString(R.string.penalty_points), false);
        checkBox_points_penalty.setChecked(penalty_points);
    }

    void settingsSave(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        penalty_points = checkBox_points_penalty.isChecked();
        editor.putBoolean(getString(R.string.penalty_points), penalty_points);
        editor.apply();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        first_option = (Button)this.findViewById(R.id.first_option);
        second_option = (Button)this.findViewById(R.id.second_option);
        thirth_option = (Button)this.findViewById(R.id.thirth_option);
        fourth_option = (Button)this.findViewById(R.id.fourth_option);
        button_report = (ImageButton)this.findViewById(R.id.button_report);
        button_exit_settings = (ImageButton)this.findViewById(R.id.button_exit_settings);
        choose_menu = (LinearLayout)this.findViewById(R.id.choose_layout);
        menu_layout = (ConstraintLayout)this.findViewById(R.id.menu_layout);
        category_layout = (ConstraintLayout)this.findViewById(R.id.category_layout);
        options_layout = (ConstraintLayout)this.findViewById(R.id.options_layout);
        button_category_basic = (Button)this.findViewById(R.id.button_category_basic);
        button_category_food = (Button)this.findViewById(R.id.button_category_food);
        button_category_kasprzak = (Button)this.findViewById(R.id.button_category_kasprzak);
        checkBox_points_penalty = (CheckBox)this.findViewById(R.id.checkBox_points_penalty);

        // Set fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //settings load
        settingsLoad();
        //old_button set
        //menu buttons
        Button start = (Button)this.findViewById(R.id.button_start);
        start.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                menu_layout.setVisibility(View.GONE);
                category_layout.setVisibility(View.VISIBLE);
            }
        });

        Button options = (Button)this.findViewById(R.id.button_options);
        options.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                options_layout.setVisibility(View.VISIBLE);
            }
        });
        //options buttons

        button_exit_settings.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                settingsSave();
                options_layout.setVisibility(View.GONE);
            }
        });


        //category buttons
        button_category_kasprzak.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
            category = "kasprzak";
            category_layout.setVisibility(View.GONE);
            choose_menu.setVisibility(View.VISIBLE);
            }
        });
        button_category_basic.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
            category = "basic";
            category_layout.setVisibility(View.GONE);
            choose_menu.setVisibility(View.VISIBLE);
            }
        });
        button_category_food.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                category = "food";
                category_layout.setVisibility(View.GONE);
                choose_menu.setVisibility(View.VISIBLE);
            }
        });
        //time options buttons
        first_option.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                //create intent
                Intent myIntent = new Intent(MainActivity.this, GameActivity.class);
                long time_to_put = 60000;
                myIntent.putExtra("time",time_to_put);
                myIntent.putExtra("category", category);
                finish();
                MainActivity.this.startActivity(myIntent);
            }
        });

        second_option.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                //create intent
                Intent myIntent = new Intent(MainActivity.this, GameActivity.class);
                long time_to_put = 120000;
                myIntent.putExtra("time",time_to_put);
                myIntent.putExtra("category", category);
                finish();
                MainActivity.this.startActivity(myIntent);
            }
        });

        thirth_option.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                //create intent
                Intent myIntent = new Intent(MainActivity.this, GameActivity.class);
                long time_to_put = 180000;
                myIntent.putExtra("time",time_to_put);
                myIntent.putExtra("category", category);
                finish();
                MainActivity.this.startActivity(myIntent);
            }
        });

        fourth_option.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                //create intent
                Intent myIntent = new Intent(MainActivity.this, GameActivity.class);
                long time_to_put = 240000;
                myIntent.putExtra("time",time_to_put);
                myIntent.putExtra("category", category);
                finish();
                MainActivity.this.startActivity(myIntent);
            }
        });

        button_report.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto",getString(R.string.email_for_reports), null));
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.title_for_report));
                startActivity(Intent.createChooser(intent, "Choose an Email client :"));
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            if(category_layout.getVisibility() == View.VISIBLE){
                category_layout.setVisibility(View.GONE);
                menu_layout.setVisibility(View.VISIBLE);
            }else if(choose_menu.getVisibility() == View.VISIBLE){
                category_layout.setVisibility(View.VISIBLE);
                choose_menu.setVisibility(View.GONE);
            }else if(options_layout.getVisibility() == View.VISIBLE){
                settingsSave();
                options_layout.setVisibility(View.GONE);
            }else{
                finish();
                System.exit(0);
            }

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}
